https://clickhouse.com/clickhouse

Ran the [docker container](https://hub.docker.com/r/clickhouse/clickhouse-server/). I had to add -p 8123:8123 to expose the browser sql interface at http://localhost:8123/play

My ulimit openfiles is way more than the ulimit flag to docker run specifies, so deleting it

Did the [Quick Start](https://clickhouse.com/docs/en/getting-started/quick-start) and [Tutorial](https://clickhouse.com/docs/en/tutorial) using the running docker container (easy!)


Added docker-compose with data volume
inserted /data/data/csv
docker-compose up -d
docker-compose exec clickhouse bash

Created clickhouse.azure.yml. THIS DOES NOT WORK IN OPENSHIFT because of random UID :(
This does work in azure :)

Created clickhouse.dhe.yml. works in OPENSHIFT but uses clickhouse/clickhouse-server:22.3 (22.8 [does not work](https://github.com/ClickHouse/ClickHouse/issues/38583))

TODO:
- look at [active-record gem](https://github.com/PNixx/clickhouse-activerecord)
- look at replication with [clickhouse-keeper](https://clickhouse.com/docs/en/guides/sre/keeper/clickhouse-keeper/)
